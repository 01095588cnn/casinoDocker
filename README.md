# Docker安装说明 CentOS 7

[TOC]

## 运行安装docker

```
$ curl -fsSL https://get.docker.com/ | sh
```

## 启用docker

```
# 设置开机启动
$ systemctl enable docker

# 启动docker
$ systemctl start docker   
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```

## 安装docker-compose

@[安装说明](https://docs.docker.com/compose/install/#install-compose)

```
# 安装
$ sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose

# 设置目录权限
$ sudo chmod +x /usr/local/bin/docker-compose

# 安装完成查询版本号
$ docker-compose --version

```

## docker-compose 命令

```
# 会优先使用已有的容器，而不是重新创建容器。
$ docker-compose up

# 每次修改过配置文件,都需要先执行此命令
$ docker-compose build

# 启动所有容器
$ docker-compose start

# 停止所有容器
$ docker-compose stop

# 使用 --force-recreate 可以强制重建容器 （否则只能在容器配置有更改时才会重建容器）
$ docker-compose up -d --force-recreate

# 停止所有容器，并删除容器 （这样下次使用docker-compose up时就一定会是新容器了）
$ docker-compose down 

# 删除所有竟象
$ docker rmi $(docker images -q)
```

## SElinx 设置

```
chcon -Rt svirt_sandbox_file_t /www/casinoDocker/mariadb
chcon -Rt svirt_sandbox_file_t  /www/casinoDocker/website
chcon -Rt svirt_sandbox_file_t  /www/casinoDocker/temp
chcon -Rt svirt_sandbox_file_t  /www/casinoDocker/log
chcon -Rt svirt_sandbox_file_t  /www/casinoDocker/conf
chmod -R 777  /www/casinoDocker/temp  /www/casinoDocker/log
chmod -R 755 /www/casinoDocker/conf

```
>-h, --no-dereference：影响符号连接而非引用的文件。
    --reference=参考文件：使用指定参考文件的安全环境，而非指定值。
-R, --recursive：递归处理所有的文件及子目录。
-v, --verbose：为处理的所有文件显示诊断信息。
-u, --user=用户：设置指定用户的目标安全环境。
-r, --role=角色：设置指定角色的目标安全环境。
-t, --type=类型：设置指定类型的目标安全环境。
-l, --range=范围：设置指定范围的目标安全环境。

## alpine 安装 vi

```
# 安装
$ apk add vi

# 删除
$ apk del vi
```

## 进入容器

```
$ sudo docker ps  
$ sudo docker exec -it 775c7c9ee1e1 /bin/bash  
```

## 查看容器IP

```
docker inspect casino_mariadb | grep IPAddress
```

## 设置系统时间更新

```
$ cp -f /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && sudo echo "Asia/Shanghai" > /etc/timezone
$ yum install ntp -y
$ systemctl enable ntpd
$ ntpdate   ntp.api.bz
$ systemctl start ntpd.service
```

## 目录写入权限设置

```
mkdir -p /www/casinoDocker/website/cache/cache/ \
/www/casinoDocker/website/cache/log/ \
/www/casinoDocker/website/cache/Runtime/ \
/www/casinoDocker/website/order/
cp /www/casinoDocker/website/web/app/member/cache/zqgq.php /www/casinoDocker/website/cache/cache/zqgq.php
cp /www/casinoDocker/website/web/app/member/cache/lqgq.php /www/casinoDocker/website/cache/cache/lqgq.php
chmod -R 777 /www/casinoDocker/website/cache/ \
 /www/casinoDocker/website/LinuxPushClient/ \
 /www/casinoDocker/website/agorder/ \
 /www/casinoDocker/website/order/ \
 /www/casinoDocker/website/usersync/ \
 /www/casinoDocker/conf/uptime/


chmod +x /www/casinoDocker/conf/uptime/UpTime_linux_amd64
chmod +x /usr/bin/supervisord
chmod +x /usr/bin/supervisorctl
chmod +x  /www/casinoDocker/website/agorder/AGOrderCLWebClient
chmod +x /www/casinoDocker/website/LinuxPushClient/PushClient.exe
chmod +x /www/casinoDocker/website/agorder/AGOrderCLWebClient
chmod +x /www/casinoDocker/website/usersync/usersync

-rw------- (600) -- 只有属主有读写权限。  
-rw-r--r-- (644) -- 只有属主有读写权限；而属组用户和其他用户只有读权限。  
-rwx------ (700) -- 只有属主有读、写、执行权限。  
-rwxr-xr-x (755) -- 属主有读、写、执行权限；而属组用户和其他用户只有读、执行权限。  
-rwx--x--x (711) -- 属主有读、写、执行权限；而属组用户和其他用户只有执行权限。  
-rw-rw-rw- (666) -- 所有用户都有文件读、写权限。这种做法不可取。  
-rwxrwxrwx (777) -- 所有用户都有读、写、执行权限。更不可取的做法。
```

## 安装mono

[说明](https://www.mono-project.com/download/stable/#download-lin-centos)

```
rpm --import "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF"
su -c 'curl https://download.mono-project.com/repo/centos7-stable.repo | tee /etc/yum.repos.d/mono-centos7-stable.repo'
yum install mono-devel

setsid mono ./PushClient.exe >/dev/null 2>&1 &
```

# 定时器 定期删除过期的文件

```
# 查询是否安装
$ crontab 

# 安装 
$ yum -y install vixie-cron crontabs

# 启动关闭重起
$ systemctl start crond.service
$ systemctl stop crond.service
$ systemctl restart crond.service

# 开机启动
$ systemctl enable crond.service

1.在vi /etc/crontab  
# Example of job definition:
# .---------------- minute (0 - 59) 分
# |  .------------- hour (0 - 23) 时
# |  |  .---------- day of month (1 - 31) 天
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ... 月
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat 星期
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed
30 5 * * 1 root find /www/casinoDocker/website/order/ -mtime +90 -exec rm -rf {} \; # 删除注单图片
40 5 * * 1,3 root find /www/casinoDocker/website/cache/log/ -type f -mtime +7 -exec rm -rf {} \; # 删除日记文件
50 5 * * * root find /www/casinoDocker/log/nginx/ -type f -mtime +1 -delete # 删除Ng日记文件
55 5 * * * root find /www/casinoDocker/log/caddy/ -type f -mtime +1 -delete # 删除caddy日记文件
*/1 * * * * root source /www/casinoDocker/srcipts/mariadb-keep.sh  # 数据库守护
30 4 * * * root source /www/casinoDocker/srcipts/mariadb-dump.sh  # 数据库会员表备份
```

## 开机启动

```
在vi /etc/rc.d/rc.local 设置开机启动
/usr/local/bin/docker-compose -f /www/casinoDocker/docker-compose.yaml start mariadb php nginx
chmod +x /usr/local/bin/docker-compose
```

## 备份数据
--socket=/var/run/mysqld/mysqld.sock
```
docker exec -it casino_mariadb mysqldump -uroot -pgyuhjjkl7698huijh78iukji78g casino_db user_list > `date +"%Y-%m-%d"`-user_list.sql
```

## 启用ROOT

```
# 启用ROOT
docker exec -it casino_mariadb mysql -uroot -pgyuhjjkl7698huijh78iukji78g -e "update mysql.user set host='%' where user='root' and host='127.0.0.1';FLUSH PRIVILEGES;"
# 禁用ROOT
docker exec -it casino_mariadb mysql -uroot -pgyuhjjkl7698huijh78iukji78g -e "update mysql.user set host='127.0.0.1' where user='root' and host='%';FLUSH PRIVILEGES;"
```