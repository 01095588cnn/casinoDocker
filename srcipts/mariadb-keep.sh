#/bin/bash
# 这是一个健康检查脚本，防止数据库自动关闭问题.
# Script description.
# This is a script that determines if the docker image is up. If not, it will launch the docker image.
# Power By Beytagh.
# 2018-8-5 16:36:50


# Determine whether to Mariadb
docker ps | grep mariadb

if [ $? -eq 0 ]; then
echo "` date +"%Y/%m/%d-%H:%M"` mariadb started." >> /www/casinoDocker/log/mariadb.log
else
cd /www/casinoDocker/;/usr/local/bin/docker-compose start mariadb
echo "` date +"%Y/%m/%d-%H:%M"` mariadb errors." >> /www/casinoDocker/log/mariadb.log
fi