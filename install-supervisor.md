## 1.安装

```
$ yum install python-setuptools
$ easy_install supervisor
```
>成功安装后可以登陆python控制台输入import supervisor 查看是否能成功加载。
/home/service/script

## 2.创建配置文件(supervisord.conf）

使用root身份创建一个全局配置文件

```
$ echo_supervisord_conf > /etc/supervisord.conf
$ supervisord -c /etc/supervisord.conf
```

## 3.修改配置文件(supervisord.conf）

```
$ vim /etc/supervisord.conf

[include]
files = /www/casinoDocker/conf/supervisor/*.conf
```

## 4.建立服务

```
$ vim /usr/lib/systemd/system/supervisord.service

[Unit]                                                              
Description=supervisord - Supervisor process control system for UNIX
Documentation=http://supervisord.org                                
After=network.target                                                

[Service]                                                           
Type=forking                                                        
ExecStart=/usr/bin/supervisord -c /etc/supervisord.conf             
ExecReload=/usr/bin/supervisorctl reload                            
ExecStop=/usr/bin/supervisorctl shutdown                            
#User=root
KillMode=process
Restart=on-failure
RestartSec=50s

[Install]                                                           
WantedBy=multi-user.target 
```

## 服务常用操作

```
$ systemctl enable supervisord # 开机启动
$ systemctl start supervisord # 启动
$ systemctl stop supervisord # 停止
$ systemctl restart supervisord # 重启
$ systemctl status supervisord # 状态
$ systemctl disable supervisord # 禁用开机启动
```

## 设置运行权限

```
sudo chmod +x /usr/bin/supervisord
sudo chmod +x /usr/bin/supervisorctl
```
