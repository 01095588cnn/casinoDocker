
/etc/caddy.d/
/usr/bin/caddy -conf /etc/caddy.conf --log stdout

## 自动TLS
tls {
    max_certs 10
}
header / Strict-Transport-Security "max-age=31536000;"

【反向代理】
proxy / localhost:4001 localhost:4002
header_upstream Host {host}
header_upstream X-Real-IP {remote}
header_upstream X-Forwarded-For {remote}
header_upstream X-Forwarded-Proto {scheme}


【白明单】
ipfilter / {
	rule allow
	ip 84.235.124.4
}
ipfilter / {
	rule block
	ip 70.1.128.0/19 2001:db8::/122 9.12.20.16
}
ipfilter / {
	rule allow
	database /data/GeoLite2.mmdb
	country US JP
}

【安装请求头】
header / {
	# Enable HTTP Strict Transport Security (HSTS) to force clients to always
	# connect via HTTPS (do not use if only testing)
	Strict-Transport-Security "max-age=31536000;"
	# Enable cross-site filter (XSS) and tell browser to block detected attacks
	X-XSS-Protection "1; mode=block"
	# Prevent some browsers from MIME-sniffing a response away from the declared Content-Type
	X-Content-Type-Options "nosniff"
	# Disallow the site to be rendered within a frame (clickjacking protection)
	X-Frame-Options "DENY"
}